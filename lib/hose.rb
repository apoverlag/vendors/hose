require "hose/version"
require 'httparty'
require 'digest/sha2'
require 'eventmachine'
require 'em-http'

module Hose

  class RestApiClient
    include HTTParty
    default_timeout 500

    def initialize(settings)
      @settings = settings
    end

    def get_response(path)
      response = request(:get,path)
      response.parsed_response
    end

    private

    def auth_headers(request_uri)
      timestamp = Time.now.utc.strftime "%Y-%m-%d %H:%M:%S UTC"
      signature_string = "#{@settings[:secret]}#{request_uri}#{timestamp}"
      signature = (Digest::SHA256.new << signature_string).to_s
      user_agent = "Ruby/#{RUBY_VERSION}"

      { "x-api-key" => @settings[:key], "x-timestamp" => timestamp, "x-signature" => signature, "user-agent" => user_agent }
    end

    def request(method, path, params={})
      params.merge!({ :headers => auth_headers(path.split('?').first) })
      self.class.send(method, "#{@settings[:url]}#{path}", params)
    end

    def process_stream(path, file=nil)
      EventMachine.run do
        http = EventMachine::HttpRequest.new("#{@settings[:url]}#{path}", :inactivity_timeout => 0).get :head => auth_headers(path.split("?").first), :keepalive => true

        http.stream do |chunk|
          unless file.nil?
            file.write chunk
            puts "\nChunk written"
          else
            puts chunk
          end
        end

        http.callback do
          p http.response_header.status
          p http.response_header
          EventMachine.stop
          file.close unless file.nil?
          return http
        end
      end

    end

  end

end
