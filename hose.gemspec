# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'hose/version'

Gem::Specification.new do |spec|
  spec.name          = "hose"
  spec.version       = Hose::VERSION
  spec.authors       = ["Martin Sabo"]
  spec.email         = ["martin@diagnosia.com"]
  spec.description   = %q{Currently the gem contains only rest api client class used in api tester and samsar
                          deploy rake tasks.}
  spec.summary       = %q{Set of rest api usage/testing utils.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"

  spec.add_dependency('httparty')
  spec.add_dependency('eventmachine')
  spec.add_dependency('em-http-request')

end
